package com.modernjava.threading;

public class FastFoodRestaurantWithSyncBlock {
	
	private String lastClientName;
	private int numbersOfBurgersSold; 
	
	public void aLongRunningProcess() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void buyBurger(String clientName) {
		aLongRunningProcess();
		synchronized(this) {
			this.lastClientName=clientName; 
			numbersOfBurgersSold++;
			System.out.println(clientName + " bought a burger" );
		}
	}

	public String getLastClientName() {
		return lastClientName;
	}

	public int getNumbersOfBurgersSold() {
		return numbersOfBurgersSold;
	}

	public static void main(String[] args) throws InterruptedException {
		long startTime = System.currentTimeMillis(); 
		FastFoodRestaurantWithSyncBlock fastFoodRestaurant= new FastFoodRestaurantWithSyncBlock();
		
		Thread t1 = new Thread(()-> {
			fastFoodRestaurant.buyBurger("Mike");
		});
		
		Thread t2 = new Thread(()-> {
			fastFoodRestaurant.buyBurger("Syed");
		});
		
		Thread t3 = new Thread(()-> {
			fastFoodRestaurant.buyBurger("Jean");
		});
		
		Thread t4 = new Thread(()-> {
			fastFoodRestaurant.buyBurger("Amy");
		});
		
		t1.start();
		t2.start(); 
		t3.start();
		t4.start();
		
		t1.join();
		t2.join();
		t3.join();
		t4.join();
		
		System.out.println("Total number of burgers sold: " + fastFoodRestaurant.numbersOfBurgersSold);
		System.out.println ("The name of last client is: " + fastFoodRestaurant.getLastClientName()); 
		System.out.println("Total execution time: " + (System.currentTimeMillis() - startTime) + " in msec"); 

	}

}




















