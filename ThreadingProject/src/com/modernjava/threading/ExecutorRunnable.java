package com.modernjava.threading;

import java.time.LocalDateTime;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class ExecutorRunnable {

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		Runnable runnableTask = () -> {
			try {
				TimeUnit.MILLISECONDS.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println (Thread.currentThread().getName() + " Finished executing at: " + LocalDateTime.now()); 
		};
		
		
		ExecutorService executor = Executors.newFixedThreadPool(10); 
		System.out.println ("First example - Executing task with execute() method"); 
		executor.execute(runnableTask);
		
		System.out.println ("Second example - Executing task with submit() method");
		Future<String> result = executor.submit(runnableTask, "COMPLETED");
		
		while (!result.isDone()) {
			System.out.println ("The method return value: " + result.get());
		}
		
		executor.shutdown();		

	}

}
