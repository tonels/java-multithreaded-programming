package com.modernjava.threading;

import java.util.stream.IntStream;

public class SumOfNumbersUsingRunnableInterfaceUsingLamda {
	public static int sum=0;
	public static int[] array= IntStream.rangeClosed(0, 5000).toArray();
	public static int total = IntStream.rangeClosed(0, 5000).sum(); 
	

	public static void main(String[] args) throws InterruptedException {
		
		Thread thread1 = new Thread(() -> {
			for (int i=0;i<array.length/2;i++) {
				add (array[i]); 
			}
		}); 
		
		Thread thread2 = new Thread(() -> {
			for (int i=array.length/2;i<array.length;i++) {
				add (array[i]); 
			}
		}); 
		
		thread1.start();
		thread2.start();
		thread1.join();
		thread2.join();
		
		System.out.println ("Sum of 5000 integers in parallel is: " + sum); 
		System.out.println ("Correct total of 5000 integers is: " + total);
		

	}
	
	public synchronized static void add (int toAdd) {
		sum = sum + toAdd; 
	}

}
