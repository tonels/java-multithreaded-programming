package com.modernjava.threading.stream;

import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

public class NumericStream {

	public static void main(String[] args) {
		//numericStreamCreationExample();
		numericStreamCommonMethods();

	}
	
	public static void numericStreamCommonMethods() {
		//System.out.println(IntStream.of(1,2,3).count()); 
		//System.out.println(IntStream.of(1,2,3).sum());
		//System.out.println(IntStream.of(1,2,3).average().getAsDouble());
		//System.out.println(IntStream.of(1,2,3).min().getAsInt());
		System.out.println(IntStream.of(1,2,3).max().getAsInt());
		
	}
	
	public static void numericStreamCreationExample() {
		/*
		 * IntStream.of(1,2,3).forEach(System.out::println); IntStream.range(4,
		 * 6).forEach(System.out::println); IntStream.rangeClosed(4,
		 * 6).forEach(System.out::println); IntStream.iterate(0,
		 * i->i+2).limit(3).forEach(System.out::println);
		 */
		
		/*
		 * LongStream.of(1,2,3).forEach(System.out::println); LongStream.range(4,
		 * 6).forEach(System.out::println); LongStream.rangeClosed(4,
		 * 6).forEach(System.out::println); LongStream.iterate(0,
		 * i->i+2).limit(3).forEach(System.out::println)
		 */;
		
		DoubleStream.of(1,2,3).forEach(System.out::println);
		LongStream.range(4, 6).asDoubleStream().forEach(System.out::println);
		LongStream.rangeClosed(4, 6).asDoubleStream().forEach(System.out::println);
		DoubleStream.iterate(0, i->i+2).limit(3).forEach(System.out::println);
		
		
		
		
	}

}
