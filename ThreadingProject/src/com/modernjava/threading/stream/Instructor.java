package com.modernjava.threading.stream;

import java.util.List;

public class Instructor {
	
	private String name; 
	private int numOfCourse; 
	private String gender; 
	private int yearsOfExperience; 
	private List<String> courses;
	
	
	public Instructor(String name, int numOfCourse, String gender, int yearsOfExperience, List<String> courses) {
		super();
		this.name = name;
		this.numOfCourse = numOfCourse;
		this.gender = gender;
		this.yearsOfExperience = yearsOfExperience;
		this.courses = courses;
	}
	
	


	@Override
	public String toString() {
		return "Instructor [name=" + name + ", numOfCourse=" + numOfCourse + ", gender=" + gender
				+ ", yearsOfExperience=" + yearsOfExperience + ", courses=" + courses + "]";
	}




	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getNumOfCourse() {
		return numOfCourse;
	}


	public void setNumOfCourse(int numOfCourse) {
		this.numOfCourse = numOfCourse;
	}


	public String getGender() {
		return gender;
	}


	public void setGender(String gender) {
		this.gender = gender;
	}


	public int getYearsOfExperience() {
		return yearsOfExperience;
	}


	public void setYearsOfExperience(int yearsOfExperience) {
		this.yearsOfExperience = yearsOfExperience;
	}


	public List<String> getCourses() {
		return courses;
	}


	public void setCourses(List<String> courses) {
		this.courses = courses;
	} 
	
	

}
