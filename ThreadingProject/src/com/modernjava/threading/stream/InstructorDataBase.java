package com.modernjava.threading.stream;

import java.util.Arrays;
import java.util.List;

public class InstructorDataBase {
	
	public static List<Instructor> getAllInstructors(){
		
		Instructor instructor1 = new Instructor("Mike", 2, "M", 10, Arrays.asList("Java Programming", "C++ Programming"));
		Instructor instructor2 = new Instructor("Syed", 1, "M", 5, Arrays.asList("Java Multi-Threaded Programming"));
		Instructor instructor3 = new Instructor("Jenny", 3, "F", 15, Arrays.asList("Java Programming", "C++ Programming", "Python Programming"));
		Instructor instructor4 = new Instructor("Rajeev", 4, "M", 6, Arrays.asList("Java Programming", "C++ Programming", "C Programming", "Python Programming"));
		
		List<Instructor> list = Arrays.asList(instructor1, instructor2, instructor3, instructor4); 
		return list;
		
	}

}
