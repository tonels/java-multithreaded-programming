package com.modernjava.threading;

import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;

public class NavigableMapsExample {

	public static void main(String[] args) {
		ConcurrentNavigableMap<Integer, String> map = new ConcurrentSkipListMap<Integer, String>(); 
		
		map.put(1, "A");
		map.put(2, "B"); 
		map.put(3, "C"); 
		map.put(4, "D"); 
		map.put(5, "E"); 
		map.put(6, "F"); 
		map.put(7, "G"); 
		map.put(8, "H"); 
		map.put(9, "I"); 
		
		System.out.println("Navigable Map: " + map); 
		System.out.println ("HeadMap (returns all < key): " + map.headMap(2)); 
		System.out.println("TailMap (returns all >= key): " + map.tailMap(2)); 
		System.out.println("SubMap (returns all between excluding the last: " + map.subMap(2, 4)); 
		

	}

}
