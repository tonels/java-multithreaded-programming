package com.modernjava.threading;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.PriorityBlockingQueue;

public class PriorityBlockingQueueExample {

	public static void main(String[] args) {
		final String[] names = {"mike", "syed", "jean", "jenny", "rajeev", "jacob", "henry"}; 
		final PriorityBlockingQueue<String> queue = new PriorityBlockingQueue<>(); 
		
		Runnable producer = () -> {
			for (String name: names)
				queue.put(name);
		};
		
		Runnable consumer = () -> {
			while(true) {
				try {
					System.out.println(queue.take());
				} catch (InterruptedException e) {
					e.printStackTrace();
				} 
			}
		};
		
		ExecutorService executor = Executors.newFixedThreadPool(2); 
		executor.submit(producer); 
		executor.submit(consumer); 
		executor.shutdown();

	}

}
