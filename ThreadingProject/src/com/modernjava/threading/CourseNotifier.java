package com.modernjava.threading;

public class CourseNotifier {

	public static void main(String[] args) {
		//Three Threads - two thread are for 2 students who are waiting for the 
		//course completion notification
		//One thread for the course intructor who is the writing the course
		
		final Course course = new Course("Java Mutlithreaded Programming");
			
		
		new Thread(() ->  {
			synchronized (course) {
				System.out.println (Thread.currentThread().getName() + "is waiting for the course: " + course.getTitle()); 
				try {
					course.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println (Thread.currentThread().getName() + " the course: " + course.getTitle() + " is now completed");
			}
			
			
		}, "StudentA").start();
		
		new Thread(() ->  {
			synchronized (course) {
				System.out.println (Thread.currentThread().getName() + "is waiting for the course: " + course.getTitle()); 
				try {
					course.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println (Thread.currentThread().getName() + " the course: " + course.getTitle() + " is now completed");
			}
			
			
		}, "StudentB").start();
		
		new Thread(() -> {
			synchronized (course) {
				System.out.println(Thread.currentThread().getName() + " is starting a new course : " + course.getTitle());
				try {
					Thread.sleep(4000);
					//course.notify();
					course.notifyAll();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
		},"Syed Ahmed").start();
		
	}

}

class Course{
	private String title; 
	private boolean completed;
	
	public Course(String title) {
		super();
		this.title = title;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public boolean isCompleted() {
		return completed;
	}
	public void setCompleted(boolean completed) {
		this.completed = completed;
	}
}
